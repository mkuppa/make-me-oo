package org.oop;

public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

        public double CalculatesDistanceBetweenFromAndTO (Point to) {
            double xDistance = to.x - x;
            double yDistance = to.y - y;
            return Math.sqrt(Math.pow(xDistance, 2) + Math.pow(yDistance, 2));
        }

        public double CalculatesAngleBetweenFromAndTO (Point to){
             double xDistance = to.x - x;
             double yDistance = to.y - y;
             return Math.atan2(yDistance, xDistance);
            }
}